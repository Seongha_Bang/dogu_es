import os


def findFile(where, name):
    result = []

    for file_name in os.listdir(where):
        file_dir = os.path.join(where, file_name)

        if os.path.isdir(file_dir):
            result.extend(findFile(file_dir, name))
        else:
            if name in file_name:
                result.append(file_dir)

    return result
