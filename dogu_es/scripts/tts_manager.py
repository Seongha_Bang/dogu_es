#!/usr/bin/env python
# coding=utf-8

import os

import rospy
from dogu_es import *
import dogu_es_msgs.msg as msg
import dogu_es_msgs.srv as srv


os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.getcwd() + "/tts-key.json"

try:
    from google.cloud import texttospeech
except ImportError as error:
    rospy.logerr("[TTSManager] Google TTS module not found")
    raise error


class TTS_Manager:
    AUDIO_CONFIG = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3
    )
    VOICE = {
        "male": texttospeech.types.VoiceSelectionParams(
            language_code="ko-KR", ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "female": texttospeech.types.VoiceSelectionParams(
            language_code="ko-KR", ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        )
    }

    def __init__(self):
        self.message_files = []
        self.reloadMessageFiles()

        rospy.init_node("tts_manager")

        self.pub = {
            "messages": rospy.Publisher("tts_manager/messages", msg.FileList, queue_size=10),
        }

        rospy.Service("tts_manager/tts", srv.PlayTTS, self.srv_playTTS)
        rospy.Service("tts_manager/message", srv.PlayMessage, self.srv_playMessage)

        rospy.wait_for_service("es_player/play")
        self.client = {
            "play": rospy.ServiceProxy("es_player/play", srv.PlayES)
        }

    def reloadMessageFiles(self):
        directory = rospy.get_param("dogu_es/tts_manager/directory")
        message_files = []

        for file_name in os.listdir(directory):
            # (name, extension) = os.path.splitext(file_name)
            message_files.append(file_name)

        self.message_files = message_files

    def createTTS(self, text, gender, target_file):
        directory = rospy.get_param("dogu_es/tts_manager/directory")

        client = texttospeech.TextToSpeechClient()
        synthesis_input = texttospeech.types.SynthesisInput(text=text)

        response = client.synthesize_speech(synthesis_input, self.VOICE[gender], self.AUDIO_CONFIG)

        with open(os.path.join(directory, target_file + ".mp3"), "wb") as f:
            f.write(response.audio_content)

    def playRequest(self, name, priority, block, loop, delay):
        result = True
        directory = rospy.get_param("dogu_es/tts_manager/directory")
        targets = []

        for file_dir in self.message_files:
            if name == os.path.splitext(file_dir)[0]:
                targets = [file_dir]
                break
            elif name in file_dir:
                targets.append(file_dir)

        if len(targets) == 0:
            rospy.logerr("[TTSManager] {} not found".format(name))
            result = False
        else:
            if len(targets) > 1:
                rospy.logwarn("[TTSManager] {} Result found - {}".format(len(targets), targets))

            self.client["play"](os.path.join(directory, targets[0]), priority, block, loop, delay)

        return result

    def pub_TTSList(self):
        data = msg.FileList()
        data.list = []

        self.reloadMessageFiles()

        for file_name in self.message_files:
            data.list.append(os.path.splitext(os.path.basename(file_name))[0])

        data.list.sort()

        self.pub["messages"].publish(data)

    def srv_playTTS(self, req):
        try:
            self.createTTS(req.text, req.voice_gender, req.file_name)

            directory = rospy.get_param("dogu_es/tts_manager/directory")
            self.message_files.append(os.path.join(directory, req.file_name + ".mp3"))
        except:
            rospy.logerr("[TTSManager] Failed to create TTS file")
            result = False
        else:
            result = self.playRequest(req.file_name + ".mp3", req.play_priority, req.play_block, req.play_loop, req.play_delay)

        return srv.PlayTTSResponse(result)

    def srv_playMessage(self, req):
        result = self.playRequest(req.file_name, req.play_priority, req.play_block, req.play_loop, req.play_delay)
        return srv.PlayMessageResponse(result)

    def main(self):
        rate = rospy.Rate(1)

        while not rospy.is_shutdown():
            self.pub_TTSList()

            rate.sleep()


if __name__ == '__main__':
    try:
        node = TTS_Manager()
        node.main()
    except rospy.ROSInterruptException:
        pass
