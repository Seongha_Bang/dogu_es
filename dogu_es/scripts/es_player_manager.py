#!/usr/bin/env python
# coding=utf-8

import subprocess as sp
import re
import os

import rospy
from dogu_es import *
import dogu_es_msgs.msg as msg
import dogu_es_msgs.srv as srv


class SoundCardInfo:
    def __init__(self, card):
        self.card = int(card)
        self.name = ""
        self.description = ""
        self.active_profile = ""
        self.profile = {}

    def addProfile(self, name, description):
        self.profile[name] = description

    def getProfileNames(self):
        return self.profile.keys()

    def getProfileDescription(self, profile):
        try:
            result = self.profile[profile]
        except KeyError:
            result = profile

        return result


class ESPlayerManager:
    FILE_TYPE = ["mp3", "wav"]

    def __init__(self):
        self.regex_patt = {
            "pacmd":
                re.compile(
                    "alsa\.card\s=\s\"(\d+)\"|alsa\.card_name\s=\s\"([^\n]+)\"|device\.description\s=\s\"([^\n]+)\"|^\t\t([^\s\n]+):\s([^\n]+)\s\(priority\s\d+,\savailable|active\sprofile:\s<([^\n]+)>",
                    re.MULTILINE
                ),
            "amixer":
                re.compile(
                    "Left[^\n]+\[(\d+)%\]",
                    re.MULTILINE
                ),
        }
        self.speaker_list = {}
        self.es_file_list = []

        rospy.init_node("es_player_manager")

        self.pub = {
            "speakers": rospy.Publisher("es_player/speakers", msg.SpeakerList, queue_size=10),
            "files": rospy.Publisher("es_player/files", msg.FileList, queue_size=10),
        }

        rospy.Service("es_player/speaker", srv.SelectSpeaker, self.srv_SelectSpeaker)

    def getSoundCardList(self):
        proc = sp.Popen(["/usr/bin/pacmd", "list-cards"], stdout=sp.PIPE)
        stdout = proc.stdout.read()

        result = {}
        card = None

        for group in self.regex_patt["pacmd"].findall(stdout):
            if group[0]:
                if card:
                    result[card.card] = card
                card = SoundCardInfo(group[0])
            elif group[1]:
                card.name = group[1]
            elif group[2]:
                card.description = group[2]
            elif group[3]:
                card.addProfile(group[3], group[4])
            elif group[5]:
                card.active_profile = group[5]

        result[card.card] = card

        return result

    def UpdateESFileList(self, event):
        result = []

        for file_dir in findFile(rospy.get_param("dogu_es/es_player/directory"), ""):
            if os.path.splitext(file_dir)[1][1:] in self.FILE_TYPE:
                result.append(os.path.basename(file_dir))
        result.sort()

        self.es_file_list = result

    def pub_SpeakerList(self, event):
        amixer = sp.check_output(["/usr/bin/amixer", "-D", "pulse", "sget", "Master"])

        data = msg.SpeakerList()
        data.volume = int(self.regex_patt["amixer"].findall(amixer)[0])
        data.list = []

        self.speaker_list = self.getSoundCardList()

        for speaker in self.speaker_list.keys():
            card = self.speaker_list[speaker]
            info = msg.SpeakerInfo()

            info.card = card.card
            info.name = card.name
            info.description = card.description
            info.active_profile = msg.ProfileInfo()
            info.active_profile.name = card.active_profile
            info.active_profile.description = card.getProfileDescription(card.active_profile)
            info.profiles = []

            for profile_name in card.getProfileNames():
                profile = msg.ProfileInfo()

                profile.name = profile_name
                profile.description = card.getProfileDescription(profile_name)

                info.profiles.append(profile)

            data.list.append(info)

        self.pub["speakers"].publish(data)

    def pub_FileList(self, event):
        self.pub["files"].publish(self.es_file_list)

    def srv_SelectSpeaker(self, req):
        profile_target = ""
        profile_list = []

        if len(req.profile) > 0 and int(req.card) in self.speaker_list.keys():
            for profile in self.speaker_list[int(req.card)].getProfileNames():
                if req.profile in profile:
                    profile_list.append(profile)

                    if "output:" in profile and len(profile) < len(profile_target):
                        profile_target = profile
                    elif len(profile_target) == 0:
                        profile_target = profile

        if len(profile_list) == 0:
            profile_list.append("")
        else:
            proc_c = sp.Popen(["/usr/bin/pactl", "set-card-profile", str(req.card), profile_target], stderr=sp.PIPE)
            proc_c.wait()

        proc_v = sp.Popen(
            ["/usr/bin/amixer", "-D", "pulse", "sset", "-Mq", "'Master'", str(req.volume) + "%"],
            stderr=sp.PIPE
        )
        proc_v.wait()

        return srv.SelectSpeakerResponse(profile_target, profile_list)

    def main(self):
        self.UpdateESFileList(None)
        rospy.timer.Timer(rospy.Duration(10), self.UpdateESFileList, False)

        rospy.timer.Timer(rospy.Duration(2), self.pub_SpeakerList, False)
        rospy.timer.Timer(rospy.Duration(1), self.pub_FileList, False)

        rospy.spin()


if __name__ == '__main__':
    try:
        node = ESPlayerManager()
        node.main()
    except rospy.ROSInterruptException:
        pass
