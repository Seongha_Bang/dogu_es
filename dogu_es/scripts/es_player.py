#!/usr/bin/env python
# coding=utf-8

import multiprocessing
import os
import psutil

import rospy
from dogu_es import *
import dogu_es_msgs.msg as msg
import dogu_es_msgs.srv as srv


def PlayES_to_PlayInfo(req=srv.PlayESRequest()):
    result = msg.PlayInfo()

    result.name = req.name
    result.file = ""
    result.priority = req.priority
    result.block = req.block
    result.loop = req.loop
    result.loop_now = 0
    result.delay = req.delay
    result.delay_now = 0

    return result


class Sound:
    def __init__(self, info=msg.PlayInfo()):
        self.info = info
        self.killed = False

        self.proc = multiprocessing.Process(target=playsound, args=(self.info.file,))
        # self.proc.daemon = True

    def play(self):
        self.proc.start()

    def stop(self):
        result = True

        try:
            psutil.Process(self.proc.pid).kill()
            self.killed = True
        except psutil.NoSuchProcess:
            result = False

        return result or self.killed

    def is_playing(self):
        return self.proc.is_alive()


class ES_Player:
    FILE_TYPE = ["mp3", "wav"]

    def __init__(self):
        self.play_list = []
        self.play_info_list = []
        self.is_all_stop = False

        rospy.init_node("es_player")

        self.pub = {
            "status": rospy.Publisher("es_player/status", msg.PlayStatus, queue_size=10),
            "list": rospy.Publisher("es_player/list", msg.PlayList, queue_size=10)
        }

        rospy.Service("es_player/play", srv.PlayES, self.srv_PlayES)

    def srv_PlayES(self, req):
        result = True

        if req.name == "":
            if req.loop == 0:
                self.is_all_stop = True
        else:
            info = PlayES_to_PlayInfo(req)
            work_dir = rospy.get_param("dogu_es/es_player/directory")
            find_result = findFile(work_dir, info.name)

            if find_result:
                info.file = find_result[0]

                if len(find_result) > 1:
                    rospy.logwarn("[ESPlayer] {} Result found - {}".format(len(find_result), find_result))
            elif os.path.isfile(info.name):
                info.file = info.name
            else:
                result = False

            if result:
                if info.file in self.play_list:
                    index = self.play_list.index(info.file)
                    del self.play_list[index]
                    del self.play_info_list[index]

                self.play_list.append(info.file)
                self.play_info_list.append(info)

        return srv.PlayESResponse(result)

    def main(self):
        play_now = None  # Type: Sound
        play_next = None  # Type: msg.PlayInfo
        block_target = None  # Type: msg.PlayInfo
        rate = rospy.Rate(1)

        while not rospy.is_shutdown():
            play_ended = []

            # Process playing status
            for info in self.play_info_list:

                # Register to play_next

                # 자신이 재생 중이지 않을 때
                if not play_now or info.file != play_now.info.file:
                    # delay 판단
                    if info.delay_now >= info.delay:
                        # block = True인 사운드가 없거나, 자신이 우선순위와 block = True 가진 사운드 일때
                        if (
                                not block_target or
                                info.file == block_target.file or
                                info.priority > block_target.priority
                        ):
                            # 다음에 재생할 사운드가 없거나 자신이 더 높은 우선순위를 가질 때
                            # 만약 우선순위가 같다면 기다린 시간이 더 긴 사운드가 우선적으로 선택됨
                            if (
                                    not play_next or
                                    info.priority > play_next.priority or
                                    (info.priority == play_next.priority and info.delay_now > play_next.delay_now)
                            ):
                                # 반복 횟수가 0이 아닐 때
                                if info.loop != 0:
                                    play_next = info

                # Enable/Disable block_target
                if info.block:
                    if block_target and info.file != block_target.file:
                        if info.priority == block_target.priority:
                            block_target.block = False

                    if not block_target or info.priority > block_target.priority:
                        block_target = info
                else:
                    if block_target and info.file == block_target.file:
                        block_target = None

                # Loop count & Stop play
                if info.loop_now >= info.loop > 0:
                    play_ended.append(info)
                elif info.loop == 0:
                    play_ended.append(info)
                    if play_now and play_now.info.file == info.file:
                        play_now.stop()
                elif not play_now or info.file != play_now.info.file:
                    info.delay_now += 1

            # When all sounds terminated by service
            if self.is_all_stop:
                self.play_list = []
                self.play_info_list = []
                self.is_all_stop = False

                if play_now:
                    play_now.stop()

                play_now = None
                play_next = None
                block_target = None

                rospy.logwarn("[ESPlayer] kill all sounds")

            # Processing variables when sound ended
            for info in play_ended:
                self.play_list.remove(info.file)
                self.play_info_list.remove(info)

                if block_target and info.file == block_target.file:
                    block_target = None

            # Stop sound
            if play_now:
                if not play_now.is_playing() or (play_next and play_next.priority > play_now.info.priority):
                    if play_now.stop():
                        rospy.logwarn("[ESPlayer] kill: {}".format(os.path.basename(play_now.info.file)))
                    else:
                        rospy.logout("[ESPlayer] Stop: {}".format(os.path.basename(play_now.info.file)))

                    play_now = None

            # Play sound
            if not play_now and play_next:
                rospy.logout(
                    "[ESPlayer] Play: {} ({}/{})".format(
                        os.path.basename(play_next.file),
                        play_next.loop_now + 1,
                        play_next.loop
                    )
                )
                play_now = Sound(play_next)
                play_now.play()
                play_next.loop_now += 1
                play_next.delay_now = 0
                play_next = None

            # Publish
            play_status = msg.PlayStatus()
            if play_now:      play_status.now = play_now.info
            if play_next:     play_status.next = play_next
            if block_target:  play_status.block = block_target
            self.pub["status"].publish(play_status)

            play_list = msg.PlayList()
            play_list.list = self.play_info_list
            self.pub["list"].publish(play_list)

            # Loop delay
            rate.sleep()

        # When ROS node end
        if play_now:
            play_now.stop()


if __name__ == '__main__':
    try:
        node = ES_Player()
        node.main()
    except rospy.ROSInterruptException:
        pass
