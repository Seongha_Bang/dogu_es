#!/usr/bin/env python
# coding=utf-8

import yaml
import os
import subprocess
import shlex
from threading import Thread as thread

import rospy
from dogu_es import *
import dogu_es_msgs.msg as msg
import dogu_es_msgs.srv as srv


class CheckScenario:
    def __init__(self, start=[""], stop=[""], kill=[""]):
        self.disable = True
        self.proc = {
            "start": {},
            "stop": {},
            "kill": {}
        }

        targets = {
            "start": start,
            "stop": stop,
            "kill": kill
        }

        for run_type in self.proc.keys():
            for target in targets[run_type]:
                self.proc[run_type][target] = [None, False]

    def runShell(self, run_type, file_name):
        split = shlex.split(file_name)

        while not self.disable:
            if not os.path.isfile(split[0]):
                split[0] = os.path.join(rospy.get_param("dogu_es/rbs_server/directory"), "scenarios", "scripts", split[0])
            proc = subprocess.Popen(split, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            proc.wait()
            self.proc[run_type][file_name][1] = proc.returncode == 0

    def start(self):
        if self.disable:
            self.disable = False

            for run_type in self.proc.keys():
                for target in self.proc[run_type].keys():
                    t = thread(target=self.runShell, args=(run_type, target,))
                    t.daemon = True
                    self.proc[run_type][target] = [t, False]
                    self.proc[run_type][target][0].start()

    def stop(self):
        self.disable = True

    def result(self, wait=False):
        result = {
            "start": len(self.proc["start"]) > 0,
            "stop": len(self.proc["stop"]) > 0,
            "kill": len(self.proc["kill"]) > 0
        }

        for run_type in self.proc.keys():
            for target in self.proc[run_type].keys():
                result[run_type] &= self.proc[run_type][target][1]

        return result


class RBS_Server:
    def __init__(self):
        self.scenario_proc = {}
        self.scenario = self.loadScenarios()

        rospy.init_node("rbs_server")

        self.pub = {
            "list": rospy.Publisher("rbs_server/list", msg.ScenarioList, queue_size=10)
        }

        rospy.Service("rbs_server/reload", srv.ScenarioReload, self.srv_ScenarioReload)
        rospy.Service("rbs_server/enable", srv.ScenarioEnable, self.srv_ScenarioEnable)

        rospy.wait_for_service("es_player/play")
        self.client = {
            "play": rospy.ServiceProxy("es_player/play", srv.PlayES)
        }

    def loadScenarios(self):
        result = {}
        directory = os.path.join(rospy.get_param("dogu_es/rbs_server/directory"), "scenarios")

        for file_name in os.listdir(directory):
            file_dir = os.path.join(directory, file_name)
            (name, extension) = os.path.splitext(file_name)

            if os.path.isfile(file_dir) and extension in [".yaml", ".yml"]:
                with open(file_dir) as f:
                    data_raw = yaml.load(f, Loader=yaml.FullLoader)

                data = {
                    "enable": False,
                    "play": {
                        "block": False,
                        "delay": 0,
                        "loop": 1,
                        "name": "",
                        "priority": 0
                    },
                    "start": [],
                    "stop": [],
                    "kill": []
                }

                data.update(data_raw)

                result[name] = msg.ScenarioInfo()
                result[name].name = name
                result[name].enable = data["enable"]
                result[name].running = False
                result[name].play_block = data["play"]["block"]
                result[name].play_delay = data["play"]["delay"]
                result[name].play_loop = data["play"]["loop"]
                result[name].play_name = data["play"]["name"]
                result[name].play_priority = data["play"]["priority"]
                result[name].start = data["start"]
                result[name].stop = data["stop"]
                result[name].kill = data["kill"]

                self.scenario_proc[name] = CheckScenario(data["start"], data["stop"], data["kill"])

        return result

    def saveScenarios(self, scenario):
        directory = os.path.join(rospy.get_param("dogu_es/rbs_server/directory"), "scenarios")

        for name in scenario.keys():
            data = {
                "enable": scenario[name].enable,

                "play": {
                    "block": scenario[name].play_block,
                    "delay": scenario[name].play_delay,
                    "loop": scenario[name].play_loop,
                    "name": scenario[name].play_name,
                    "priority": scenario[name].play_priority
                },

                "start": scenario[name].start,
                "stop": scenario[name].stop,
                "kill": scenario[name].kill
            }

            with open(os.path.join(directory, name + '.yaml'), 'w') as f:
                yaml.dump(data, f)

    def playRBS(self, play=True, scenario=msg.ScenarioInfo()):
        loop = [0, scenario.play_loop]

        self.client["play"](
            scenario.play_name,
            scenario.play_priority,
            scenario.play_block,
            loop[int(play)],
            scenario.play_delay,
        )

    def srv_ScenarioReload(self, req):
        self.scenario = self.loadScenarios()
        return srv.ScenarioReloadResponse()

    def srv_ScenarioEnable(self, req):
        result = True

        if req.name in self.scenario.keys():
            self.scenario[req.name].enable = req.enable
            self.scenario[req.name].running &= req.enable
            self.saveScenarios(self.scenario)
        else:
            result = False

        return srv.ScenarioEnableResponse(result)

    def main(self):
        rate = rospy.Rate(1)

        while not rospy.is_shutdown():
            for key in self.scenario.keys():
                if self.scenario[key].enable:
                    self.scenario_proc[key].start()
                    proc_result = self.scenario_proc[key].result()

                    if self.scenario[key].running:
                        if proc_result["stop"]:
                            self.scenario[key].running = False

                        if proc_result["kill"]:
                            self.playRBS(False, self.scenario[key])
                    else:
                        if proc_result["start"]:
                            self.scenario[key].running = True
                            self.playRBS(True, self.scenario[key])
                else:
                    self.scenario_proc[key].stop()
                    self.playRBS(False, self.scenario[key])

            msg_list = msg.ScenarioList()
            msg_list.list = self.scenario.values()
            self.pub["list"].publish(msg_list)

            rate.sleep()

        self.saveScenarios(self.scenario)
        for key in self.scenario.keys():
            self.scenario_proc[key].stop()


if __name__ == '__main__':
    try:
        node = RBS_Server()
        node.main()
    except rospy.ROSInterruptException:
        pass
