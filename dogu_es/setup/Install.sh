#!/bin/bash

echo "[Setup] Install google-cloud-storage"
pip install google-cloud-storage
echo "[Setup] Done"

echo
echo "[Setup] Add URI for cloud SDK"
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee /etc/apt/sources.list.d/google-cloud-sdk.list
echo "[Setup] Done"

echo
echo "[Setup] Install apt-transport-https"
sudo apt-get install apt-transport-https ca-certificates gnupg
echo "[Setup] Done"

echo
echo "[Setup] Get public key about google cloud"
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
echo "[Setup] Done"

echo
echo "[Setup] Update package list"
sudo apt-get update >/dev/null
echo "[Setup] Done"

echo
echo "[Setup] Install Google Cloud SDK"
sudo apt-get install google-cloud-sdk
echo "[Setup] Done"

echo
echo "[Setup] Install texttospeech"
pip install --upgrade google-cloud-texttospeech
echo "[Setup] Done"

echo
echo "[Setup] Copy TTS Key JSON File"
DIR=$(dirname $(realpath -s ${BASH_SOURCE}))
cp ${DIR}/tts-key.json ~/.ros/tts-key.json
echo "[Setup] Done"

